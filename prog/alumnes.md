---
layout: page
title: "Alumnes i els seus portafolis"
---

Links de l'alumnat:

- [Esthefany](https://openprocessing.org/user/343176/?view=sketches&o=1)

- [Pau](https://openprocessing.org/user/343180?view=sketches&o=2)

- [Chloe](https://openprocessing.org/user/343175?view=sketches&o=2)

- [Bruno](https://openprocessing.org/user/343172/?view=sketches)

- [David](https://openprocessing.org/user/344449/?view=sketches&o=1)

- [Angel](https://openprocessing.org/user/343174?view=sketches&o=1)

- [Aiden](https://openprocessing.org/user/343850?view=sketches&o=2)

- [Arwen](https://openprocessing.org/user/343179?view=sketches&o=1)

- [Emily](https://openprocessing.org/user/343846/?view=sketches)

- [Ferran](https://openprocessing.org/user/345087?view=sketches)

- [Axel](https://openprocessing.org/user/343852?view=sketches&o=1)

- [Sofia](https://openprocessing.org/user/345660?view=sketches&o=1)

- [Raza](https://openprocessing.org/user/343857/?view=sketches&o=2)

- [Zean](https://openprocessing.org/user/345085?view=sketches&o=6)

- [Simon](https://openprocessing.org/user/347589/?view=sketches#topPanel)

- [Nayara](https://openprocessing.org/user/361523/?view=sketches&o=1)

- [Noel](https://openprocessing.org/user/345659?view=sketches&o=6)

- [Adrià](https://openprocessing.org/user/361536/?view=sketches&o=1)
