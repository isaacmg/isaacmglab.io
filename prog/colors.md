---
layout: page
title: "Colors i text"
---

## Colors i Text

Els colors són una part molt important del llenguatge de programació Processing. Per això és important donar-li la seva importància.

Els colors combinen entre sí i no podem utilitzar qualsevol combinació de colors.

En aquesta activitat hauràs d'escollir 5 combinacions de colors que t'agradin.

Podeu escollir vosaltres la combinació de colors que us agradin o utilitzar eines que us ajuden a veure quines combinen millor.

Eines que podeu utilitzar:

- [Coolors](https://coolors.co)
- [Colourhunt](https://colorhunt.co/)

Podeu navegar per aquestes dues pàgines i buscar o crear paletes de colors que us agradin. N'heu d'escollir 5.

Molt bé. Quan tingueu la primera combinació que us agradi, creareu un sketch de Processing amb tres quadrats amb els colors que indiquen i un pel color de fons de l'sketch.

Us poso un exemple!

A mi m'agrada paleta: [LINK](https://colorhunt.co/palette/f9ed69f08a5db83b5e6a2c70)

Si la visiteu, al posar el ratolí sobre cada un dels colors, abaix apareix unes lletres. Aquestes lletres identifiquen el color. De l'sketch que jo he seleccionat, aquests són els colors:

- #F9ED69
- #F08A5D
- #B83B5E
- #6A2C70

Aquests colors estan en format Hexagesimal. Nosaltres els volem en format RGB, que ja us vaig explicar que és un acrònim de Red Green Blue.

Podeu visitar aquesta pàgina: [LINK](https://www.rapidtables.com/convert/color/hex-to-rgb.html) que us transformarà els caracters en Hexagesimal a RGB. No heu de posar l'almohadeta (#).

El primer nombre (#F9ED69), a mi me'l tradueix a:

- 249, 237, 105

Aquests nombres sí que els puc escriure en l'sketch de Processing.

Ara podeu traduir tots els colors de la paleta de colors que heu escollit i crear l'sketch amb els 3 quadrats.

Us deixo un exemple que he fet jo: [LINK](https://editor.p5js.org/imuro/sketches/Ja27GXCFO)
