function setup() {
  createCanvas(200, 200);
}

function draw() {
  background(220);
  
  // Dibuixar figures, fill, stroke
  // Set ellipses and rects to CENTER mode
  ellipseMode(CENTER);
  rectMode(CENTER);
  
  // Draw Zoog's body
  stroke(0);
  fill(175);
  rect(mouseX,mouseY,20,100);
  // Draw Zoog's head
  stroke(0);
  fill(255);
  ellipse(mouseX,mouseY-30,60,60);
  
  // Draw Zoog's eyes
  fill(mouseX,0,mouseY);
  ellipse(mouseX-19,mouseY-30,16,32);
  ellipse(mouseX+19,mouseY-30,16,32);

  // Draw Zoog's legs
  stroke(0);
  line(mouseX-10,mouseY+50,pmouseX-10,pmouseY+60);
  line(mouseX+10,mouseY+50,pmouseX+10,pmouseY+60);
  
  // mouseX, mouseY
  // mousePressed, keyPressed
}
function keyPressed() {
  print("Take me to your leader!");
}
function mousePressed() {
  stroke(0);
  fill(255);
  ellipse(30,30,60,60);
}