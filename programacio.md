---
layout: page
title: "Programació i Robòtica"
---
# Programació i Robòtica a 3r ESO

Aquí podreu trobar els materials que anirem utilitzant a l'assignatura.

# Índex

1. [Portafoli d'alumnes](prog/alumnes)

2. PRESENTACIONS!

 * [![Contingut](/assets/img/processing/processingintroduccio.png)](prog/slides/introduccio.html)

 * [![Contingut](/assets/img/processing/processingsetupdraw.png)](prog/slides/setupdraw.html)

 * [![Contingut](/assets/img/processing/processingrandomnoise.png)](prog/slides/randomnoise.html)

 * [Colors](prog/colors)

 * [![Contingut](/assets/img/processing/processingcondicions.png)](prog/slides/condicions.html)

 * [![Contingut](/assets/img/processing/processingbouncingball.png)](prog/slides/bouncingball.html)

 * [![Contingut](/assets/img/processing/processingelseif.png)](prog/slides/elseif.html)

 * [![Contingut](/assets/img/processing/processingtaulerdibuix.png)](prog/slides/taulerDeDibuix.html)

 * [![Contingut](/assets/img/processing/processingbucles.png)](prog/slides/bucles.html)

 * [![Contingut](/assets/img/processing/processingbucles2.png)](prog/slides/bucles2.html)

 * [![Contingut](/assets/img/processing/processingbuclesanidados.png)](prog/slides/buclesanidados.html)

* [![Contingut](/assets/img/processing/processingarrays.png)](prog/slides/arrays.html)

* [![Contingut](/assets/img/arduino/blink.png)](prog/slides/blinkArduino.html)

* [![Contingut](/assets/img/arduino/hcsr04.png)](prog/slides/hcsr04Arduino.html)

* [![Contingut](/assets/img/arduino/ledp5j5.png)](prog/slides/ledp5js.html)

* [![Contingut](/assets/img/arduino/ldr05Portada.png)](prog/slides/ldr05.html)

* [![Contingut](/assets/img/arduino/hcsr04p5js.png)](prog/slides/hcsr04p5js.html)

* [![Contingut](/assets/img/arduino/sensorSo3.png)](prog/slides/microfonArduino.html)

3. ShowCase
 - [Okazz](https://twitter.com/okazz_/status/1627666702627667970?t=Oq1hD4cfboQokE7lPO0ARg&s=35)


Llista d'entregues que haurieu de tenir en el vostre usuari d'OpenProcessing.

-  E1: Link del vostre usuari a OpenProcessing
-  E2: Cara en blanc i negre
-  E3: Cara expressant un sentiment
-  E4: Paleta de colors
-  E5: Utilitza la funció random
-  E6: Utilitza les variables width i height en un sketch de forma creativa
-  E7: Utilitza les variables per canviar els valors de les figures. Change.
-  E8: Tauler de dibuix. Ha de tenir al menys 5 tecles i el ratolí ha de fer alguna cosa al apretar.  
-  E9: Bola rebotant a les parets de l'sketch.


4. VIDEOS

Formes i figures:
{% include youtubePlayer.html id="c3TeLi6Ns1E" %}

Color:
{% include youtubePlayer.html id="riiJTF5-N7" %}
