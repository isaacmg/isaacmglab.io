---
layout: page
title: "Programació"
---

En aquesta pàgina recullo les presentacions que he anat fent sobre programació. Feu-los vostres!

#### Programació

<div class="space"/>
<div class="row">
  {% include cards.html link="/programacio" src="/assets/img/processing/processingintroduccio.png" title="Processing" text="Llista de presentacions utilitzades per explicar Processing a l'alumnat de 3r " footer="També hi han coses d'Arduino" %}

  {% include cards.html link="/sonic-pi" src="/assets/img/sonicpi/sonicpilogo.png" title="Sonic Pi" text="Llista de presentacions utilitzades per explicar Sonic Pi a l'alumnat de 3r" footer="Portat a l'aula!" %}



</div>
