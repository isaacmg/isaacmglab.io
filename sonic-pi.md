---
layout: page
title: "Sonic Pi"
---
# Sonic Pi a 3r ESO

Aquí podreu trobar els materials que anirem utilitzant a l'assignatura.

# Índex

1. PRESENTACIONS!

 * [![Contingut](/assets/img/sonicpi/sonicpiintroduccio.png)](sonicpi/slides/introduccio.html)

 * [![Contingut](/assets/img/sonicpi/sonicpiusesynth.png)](sonicpi/slides/useSynth.html)

 * [![Contingut](/assets/img/sonicpi/sonicpiritme.png)](sonicpi/slides/ritme.html)
