---
layout: page
title: "Activitats"
---



<div class="space"/>
<div class="row">
  {% include cards.html link="https://www.youcubed.org/task-grades/12/" src="https://pbs.twimg.com/profile_images/1067185852227346432/uhdwOmX6_400x400.jpg" title="Youcubed" text="Youcubed ofereix 12 activitats de terra baix-sostre alt per poder portar a l'aula." footer="En anglès! 1r, 2n ESO" %}

  {% include cards.html link="https://www.euclidea.xyz/" src="https://play-lh.googleusercontent.com/wyooO21wqaQ-kVPSPfDaTUfc1zF38fS3iGeCbCqUUTYH1Xj_W2W1u8rxLaoLUFKPYJg" title="Euclidea" text="Pensar com trobar figures geomètriques" footer="En anglès! Ordinador i connexió. Tots els nivells!" %}

</div>
<div class="space"/>
<div class="row">
  {% include cards.html link="https://nrich.maths.org/summer-secondary-posters" src="https://educationalliance.ca/wp-content/uploads/2020/04/logo-2012-500x500-1.png" title="Secondary Posters Nrich" text="Activitats per reflexionar i modelar a secundària" footer="En anglès!" %}

  {% include cards.html link="https://wodb.ca" src="https://wodb.ca/images/wodb_logo.jpg" title="Which One Doesn't Belong?" text="Argumentar pertinença en un grup.  <a href='https://mrrowe.com/wodb/'>Més exercicis</a>" footer="En anglès!" %}

</div>
<div class="space"/>
<div class="row">
  {% include cards.html link="https://sites.google.com/view/mesmates/edicions-anteriors?authuser=0" src="https://agora.xtec.cat/cesire/wp-content/uploads/usu397/2019/12/logo_mes_mates.png" title="Més Mates" text="Recull de problemes del concurs + Mates" footer="En català. 3r i 4r ESO" %}

  {% include cards.html link="ademgi.html" src="https://i.pinimg.com/originals/9d/83/e2/9d83e20a0ef3d87966d070f56af93e07.png" title="ademgi" text="Recull de problemes d'Ademgi" footer="En català. Tots els nivells" %}

</div>
<div class="space"/>
<div class="row">
  {% include cards.html link="https://whenmathhappens.com/3-act-math/" src="http://curiousthoughtsofeb.com/wp-content/uploads/2018/02/dan.png" title="3 actes" text="Problemes de 3 actes. <a href='https://docs.google.com/spreadsheets/d/1jXSt_CoDzyDFeJimZxnhgwOVsWkTQEsfqouLWNNC6Z4/pub?output=html'>Més problemes</a>" footer="En anglès. Tots els nivells" %}

  {% include cards.html link="cangur.html" src="http://www.aksf.org/res/kangaroo-logo.png" title="3 actes" text="Problemes de Cangur" footer="En català. Tots els nivells" %}

</div>
<div class="space"/>
<div class="row">
  {% include cards.html link="https://pbalaguer19.github.io/visualPatterns/" src="https://dr282zn36sxxg.cloudfront.net/datastreams/f-d%3Acf659196306d19ff15fb1aa47b0d9e66c66131f018e2cb8d4bc3be6b%2BIMAGE_TINY%2BIMAGE_TINY.1" title="Patrons visuals" text="Trobar patrons visuals. <a href='http://www.visualpatterns.org/'>Més patrons</a>" footer="En català. Tots els nivells. Ordinador i connexió" %}

  {% include cards.html link="https://estimation180.com" src="https://pbs.twimg.com/profile_images/766505259707412480/rR23XEed.jpg" title="Estimation 180" text="Problemes d'estimació" footer="En anglès. Tots els nivells" %}

</div>
<div class="space"/>
<div class="row">
  {% include cards.html link="http://fractiontalks.com" src="https://mathforlove.com/wp-content/uploads/2019/09/fraction-talks.jpg" title="Fraction Talks" text="Problemes sobre fraccions. <a href='https://spring-of-mathematics.tumblr.com/post/171147163924/this-is-a-picture-of-100-of-my-solutions-in-each'>Més fraccions</a>" footer="En anglès. Tots els nivells." %}


</div>
