---
layout: post
title: "Matemàtiques per a baixa visió"
categories: [Matemàtiques]
tags: [matemàtiques, visió]
date: 2022-09-12
comments: true
---

## Introducció

Com m'he trobat amb més casos dels que esperava, he fet un recull de recursos per treballar amb l'alumnat amb baixa visió.

* [Matemàtiques](https://www.symbaloo.com/mix/matematiques149)
* [Sumes](assets/recursos/baixavisio/sumes1.odt)
* [Material didáctico de operaciones](https://adimir.org/material-didactico-operaciones-matematicas/)
