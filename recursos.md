---
layout: page
title: "Recursos"
---

# Materials Matemàtiques
- [Mat3](https://mat3.cat/materials/guies-de-treball/)
- [Materials de Anton Aubanell](http://www.xtec.cat/~aaubanel/)
- [Los tres actos de los enteros](https://tierradenumeros.com/post/hilo-tres-actos-de-los-enteros/),[Dan Meyer 3 Acts](https://docs.google.com/spreadsheets/u/0/d/1jXSt_CoDzyDFeJimZxnhgwOVsWkTQEsfqouLWNNC6Z4/pub?output=html)
- [Drive amb activitats](https://drive.google.com/drive/folders/1DRckVb0tHEqQqFpH1zH4rd0zaxBOexpl)
- [Videconferencies Cesire](https://agora.xtec.cat/cesire/general/videoconferencies-del-curs-activitats-riques-i-competencies-matematiques-a-laula-de-secundaria-en-obert/)
- [Llicències d'estudi](https://agora.xtec.cat/cesire/ambit-matematic/innovacio-i-recerca_mat/llicencies-destudi_mat/#Competencia_matematica_i_geometria_entre_l8217ESO_i_el_Batxillerat)
- [Buscador d'Nrichs](https://nrich.maths.org/search/?search=broads+topics&tab=1&fs=111110000000111)
- [Enlaces útiles para la clase de matemáticas](https://docs.google.com/document/d/174DY7xK8rLVvVwqYCweQDZ0tO4Zluf8RiG2k-cTWPDw/edit)
- [Tecnologia a la clase de mates](https://mei.org.uk/integrating-technology)
- [Math Tasks/Resources](https://docs.google.com/document/d/1vk4UNSkYUXtcL7znqpxRQD_9h7Fa9cV8wPuWOmDtHoI/edit)
- [Mariel Mates](https://marielmatesblog.wordpress.com)
- [Materials Genially](https://view.genial.ly/5f5e5dcd79626a0d71280042)
- [Matemàtiques BAT1-Científic/Tecnològic](https://docs.google.com/document/d/1YCm2M9XL3PwRna-7okh0lCBzn0o8pyvwR2xPEAglu2M/edit#heading=h.4jvlyd3kj3wt)
- [Àrees i Volums](https://drive.google.com/file/d/1Y6rgT6OopZEKROyxYC5BH21SgkOWpY3-/view)
- [Llibres Mates 1r ESO](https://drive.google.com/drive/folders/1BZG7rhk_aJg2q_mMo5_Jc1YR2BZORBZE)
- [K-12 Math Collection: 3-Act Tasks/Desmos Activities/Thinking Prompts](https://drive.google.com/drive/folders/1VR1wyM6W01dA7iKjRO7VWqLo9Z4Fznoz)
- [YouCubed Tasks](https://www.youcubed.org/tasks/)
- [EducaMadrid M Dominguez](https://mediateca.educa.madrid.org/usuario/mdominguezromero)
- [Recursos de interes](https://sites.google.com/view/matesconsara/otros-recursos-de-inter%C3%A9s?authuser=0)
- [Math Resources](https://docs.google.com/spreadsheets/d/1bIOh3S-bN0LCjVWrfpnhkaIapZKwtbAr5Z1gJQ_KGEY/edit#gid=0)
- [Desmos Lola Morales](https://sites.google.com/view/desmosteacher-lolamorales)
- [Recursos per a l'aula de matemàtiques](https://serveiseducatius.xtec.cat/cesire/panoramica/recursos-per-laula-de-matematiques/)
- [Activitats Pràctica Productiva Abeam](https://docs.google.com/spreadsheets/d/1oSZED2zX-27ApyogEwX5rseBxrCoKWoD/edit#gid=767189848)
- [Thinking Mathematically](https://nrich.maths.org/mathematically) (Activitats per secundaria)
- [Mates 2 ESO](https://view.genial.ly/613b711ac807b70da80d687f)
- [Videos de P Triviño](https://twitter.com/p_trivino/status/1474139615489675265?t=bwK9g-CP2WwiO0RoJdJ0AQ&s=09)
- [Videos del Creamat](https://www.youtube.com/user/CREAMAT1)
- [Interwoven Maths Nathan Day](https://interwovenmaths.com/tagged/booklets)
- [Dr Austin Maths](https://www.draustinmaths.com/)

#### Geogebra
- [Activitats autoavaluables Javier Cayetano](https://www.geogebra.org/m/AsMKtWd4)
- [Competencias Matemáticas 1º ESO](https://www.geogebra.org/m/dEV5qYNY)
- [Competencias Matemáticas 2º ESO](https://www.geogebra.org/m/aFeyvgJK)
- [Competencias Matemáticas 3º ESO](https://www.geogebra.org/m/mygg2z2f)
- [Geogebra Triviño](https://geogebra.ptrivino.es/export/capital201.html),[2](https://geogebra.ptrivino.es/export/capital202.html),[3](https://geogebra.ptrivino.es/export/capital203.html),[4](https://geogebra.ptrivino.es/export/capital204.html),[5](https://geogebra.ptrivino.es/export/capital205.html),[6](https://geogebra.ptrivino.es/export/capital206.html),[7](https://geogebra.ptrivino.es/export/capital207.html),[8](https://geogebra.ptrivino.es/export/capital208.html),[9](https://geogebra.ptrivino.es/export/capital209.html), [2º BAT](https://www.geogebra.org/m/rr7qys9m)
- [Geogebra Rafael](https://www.geogebra.org/u/rafael)
- [Tutorial Geogebra](https://www.geogebra.org/m/depdxhks)
- [Tutorials Geogebra](https://www.geogebra.org/a/14)
- [Learn Geogebra](https://www.geogebra.org/m/jkprzjyb)

#### Raonament i prova
- [minilliçons](https://puntmat.blogspot.com/2011/11/minillicons-i-estrategies.html), [2](https://puntmat.blogspot.com/2014/03/mes-sobre-minillicons.html), [Pràctica productiva: equacions de primer grau](https://puntmat.blogspot.com/2018/09/practica-productiva-equacions-de-primer.html), [sistemes d'equacions](https://puntmat.blogspot.com/2015/01/practica-productiva-i-sistema-dequacions.html), [equacions de segon grau](https://puntmat.blogspot.com/2014/03/practica-productiva-i-equacions-de.html), [Visualització amb cubets](https://puntmat.blogspot.com/2012/02/visualitzacio-amb-cubets-iii.html), [WODB o QUELI](https://sites.google.com/xtec.cat/cesire-matematiques-campanyes/inici/dimensi%C3%B3-web/wodb?authuser=0), [Igual, però diferent](https://sites.google.com/xtec.cat/cesire-matematiques-campanyes/inici/dimensi%C3%B3-web/igual-o-diferent?authuser=0),[WODB](http://wodb.ca/) ,[Cubets encaixables - Cubets](https://sites.google.com/xtec.cat/cesire-matematiques-campanyes/inici/laboratori-de-matem%C3%A0tiques/cubets-encaixables-vistes?authuser=0), [simulador de blocs](http://www.fisme.science.uu.nl/toepassingen/28020/), [Tallar i multiplicar](https://calaix2.blogspot.com/2012/10/tallar-i-multiplicar.html), [Largest Product](https://nrich.maths.org/1785).

- [Funcions lineals](https://teacher.desmos.com/activitybuilder/custom/5f3cf2755af60a35eb104501?lang=es), [1](https://twitter.com/marielgm9/status/1496777854721626116?t=WlQOAByx_cJaQUT9nlSUEg&s=09), [1](https://twitter.com/nathanday314/status/1619036758158761984?t=prRDJCA15WfsbK8b4-mdtw&s=35)

 [Funcions Lineals](https://x.com/lolamenting/status/1787823818662162436?t=Tn_wYri7dcioxjMON6IsVw&s=35)

- [Prealgebra](https://www.solvemoji.com/popular), [1](https://teacher.desmos.com/activitybuilder/custom/621712cb287cce77fd6f90d1?lang=es), [2](https://mathszone.co.uk/resources/grid/ooodle/), [3](https://twitter.com/pbeltranp/status/1494345687961677832), [4](https://mathszone.co.uk/resources/grid/ooodlemax/), [5](https://teacher.desmos.com/activitybuilder/custom/620c0a7de8315a6ed916279b?lang=es), [6](https://twitter.com/smaccarrone/status/1580187467583324166)
- [Equacions de primer grau amb balances](https://quevamosahacerhoy.com/ecuaciones-de-primer-grado-con-la-balanza/?utm_source=blogsterapp&utm_medium=twitter), [1](https://twitter.com/nathanday314/status/1610325929619767298?t=MVlMO7y30iQRsOS4u1Ff4A&s=35)
- [Algebra i geometria](https://twitter.com/lolamenting/status/1496426997001641986?t=AaG9o8JTej3g9w-ZvTnh6g&s=09)
- [Potencies](https://twitter.com/smaccarrone/status/1454115852773638144?t=J042anjWQdf_Sps4jtPnfQ&s=09), [1](https://twitter.com/lolamenting/status/1451462921666678809?t=XouUusYGTziUAaLKXJujXg&s=09)
- [Sistemes d'equacions](https://twitter.com/pbeltranp/status/1444589336759001095?t=a_GfkpU2w26kv4TlYPkYCQ&s=03), [1](https://twitter.com/DeulofeuJordi/status/1238897947829587972?s=09)
- [Balances d'equacions](https://teacher.desmos.com/activitybuilder/custom/6011c8ac0cf5ee4473203b36?lang=es),[2](https://www.didax.com/math/virtual-manipulatives.html),[3](https://www.dlt.ncssm.edu/tiger/math1.htm), [4](https://mathigon.org/polypad),[4](https://phet.colorado.edu/sims/html/equality-explorer/latest/equality-explorer_es.html),[5](https://www.transum.org/software/SW/Starter_of_the_day/Students/Stable_Scales_Quiz.asp)
- [Patrons visuals](https://pbalaguer19.github.io/visualPatterns/)
- [Juegos y Desafios Matematicos](https://juegosydesafiosmatematicos.com/)


#### Estadística i probabilitat
[Stats](https://twitter.com/nathanday314/status/1624851235781111808?t=rbeqw8v6P39HxnU12Y0bxA&s=35), [1](http://www.s253053503.websitehome.co.uk/msv/index.html)
- [Col·lecció d'activitats a Desmos](https://teacher.desmos.com/collection/60a295dace195c38b21c139a?lang=es)
- [Toda la estadistica de secundaria con geogebra](https://www.youtube.com/watch?v=0TloqsuEGHA), [1](https://drive.google.com/file/d/124SPE-PwvGB78O73eTJeU_ns5LNOHY_Y/view)
- [Unequal averages](https://nrich.maths.org/unequal), [1](https://www.youtube.com/watch?v=BhDINcErhkU&t=7329s), [2](https://twitter.com/CcBcnMvd/status/1401157091621052418/photo/1), [3](https://twitter.com/CcBcnMvd/status/1401157091621052418)
- [Estadística i probabilitat FeemCat](http://ademgi.feemcat.org/materials/tema13/)
- [Probabilidad](https://seeing-theory.brown.edu/basic-probability/es.html), [1](https://twitter.com/smaccarrone/status/1321773822878584834?s=09)
- [Probabilidad](https://mediateca.educa.madrid.org/video/mazp1eunq7j9or73), [2](https://mediateca.educa.madrid.org/video/vuvyumtxrz74y2h2), [3](https://mediateca.educa.madrid.org/video/ce536ahzdgwcds77)
- [Tareas ricas Probabilidad](https://docs.google.com/presentation/d/1tPsgxgObWB1TCmcq6Ugv06sDMfhbYdSUu3J2qCNr11A/edit#slide=id.ge4fc03a361_0_408)
- [Simulador de probabilidad Processing](http://matematicas11235813.luismiglesias.es/2021/03/31/simulador-de-probabilidad-lanzamiento-de-un-dado-con-proccesing/), [1](https://openprocessing.org/sketch/1154275)
- [Combinatoria](https://www.cambridgemaths.org/Images/espresso_31_introduction-to-combinations.pdf)
- [Correlación no es causalidad](https://twitter.com/Picanumeros/status/1316810927262572546), [2](https://picanumeros.wordpress.com/2020/10/15/el-secreto-de-las-spurious-correlations-por-que-comer-queso-no-causa-la-aparicion-de-doctores-en-ingenieria/)
- [Titula la noticia](https://teacher.desmos.com/activitybuilder/custom/5f7016a742f2347c15934f5f?lang=es#preview/fc7e4e69-6d6c-4dc8-acfb-afab095cda7b)
- [Graphing Stories](https://teacher.desmos.com/activitybuilder/custom/58797d35d81a612605304b1f?lang=es&collections=featured-collections%2C5da6476150c0c36a0caf8ffb), [1](http://www.graphingstories.com/), [2](https://teacher.desmos.com/activitybuilder/custom/62207d7ee850be0a20fd1c7e?lang=es)
[Grafics](https://twitter.com/pbeltranp/status/1286962047293493248?t=IUoeu6YTsq5mPeF5JU73xg&s=09), [1](https://slowrevealgraphs.com/), [2](https://www.cienciaoberta.cat/grafics/)
- [Analitzant Grafics](https://www.youcubed.org/resources/analyzing-graphs-3-12/)
- [Beano](https://twitter.com/palindromiano/status/1446518464810819601)
- [Datos para entender el mundo](https://abajoradicales.blogspot.com/2017/07/datos-para-entender-el-mundo.html)
- [Epidemic](https://nrich.maths.org/epidemic)
- [Mapes](https://twitter.com/joancamp/status/1233159991495921670?s=09)

- [Trigonometria](https://twitter.com/MathigonOrg/status/1398640589823004677?s=09), [1](https://www.geogebra.org/m/rgcmdtd5), [2](https://teacher.desmos.com/activitybuilder/custom/5f23cca2e49e0244f13c9b1a?lang=es), [3](https://twitter.com/totipm/status/1234194987241349120?s=09), [trigonometria geogebra](https://www.geogebra.org/m/fDrEtZ6b), [4](https://twitter.com/af_bertotomas/status/1466853030217793536?t=m-BcyA_IqUEycSyv6JpFhg&s=09)

#### Aritmètica
- [Mastermind](https://twitter.com/virgi_carmona/status/1473383186600280070?t=Rt9apYqDMMJ98lHf0U-JCA&s=09), [1](https://docs.google.com/spreadsheets/d/1Y_IAPOUj1M0FD4qNSbEFLZNTPbChcfmGTU36GHVc2JI/edit#gid=1454487176)
- [Los cuatro cuatros](https://es.wikipedia.org/wiki/Cuatro_cuatros)
- [Parentesis](https://twitter.com/lolamenting/status/1443242320837611536?s=03),[1](https://www.coolmathgames.com/0-make-24), [2](https://teacher.desmos.com/activitybuilder/custom/57ae458a697f767c75597801)
- [Sumes](https://transum.org/Software/SW/Starter_of_the_day/starter_November13.ASP?sums=2)
- [Los numeros y la humanidad](http://gisme.eu/exponumhum/es/counters.php)
- [Problemas de Fermi](https://polipapers.upv.es/index.php/MSEL/article/download/7707/8120), [1](https://detalesanewton.wordpress.com/2019/11/09/un-discurso-y-dos-problemas-de-fermi-sobre-el-calentamiento-global/comment-page-1/#comment-2632)
- [Fraccions](https://twitter.com/pbeltranp/status/1384591092230787086?s=09), [1](https://twitter.com/DavidBarba2/status/1375520549414244357?s=09), [2](https://nrich.maths.org/13081), [3](https://twitter.com/MsIdeasMnosCtas/status/1284077505125789696/photo/1), [4](http://fractiontalks.com/), [5](https://twitter.com/lolamenting/status/1480527305709662220?t=bBNp4ONmRsZs5_xon75d2g&s=09), [6](https://drive.google.com/file/d/1ZofRv3RWD2MoCDtVHa8IuQ24qx_gDpRY/view), [6](https://drive.google.com/file/d/1inU3lXQoEnrl0qbLMaJeg3LahNXA-11T/view)
- [Fraccions 2](https://twitter.com/lolamenting/status/1467387620959391746?t=a3NWvt1PU-LEuUBq6lIOnw&s=09), [1](https://teacher.desmos.com/activitybuilder/custom/61ac52448d54530a3d3beab1?lang=es), [2](https://teacher.desmos.com/activitybuilder/custom/61ac6b3e91c28f0a5c4d3f54?lang=es), [3](https://teacher.desmos.com/activitybuilder/custom/61ac85848d54530a3d3bed25?lang=es), [4](https://teacher.desmos.com/activitybuilder/custom/61ac895a183d844be434fe6c?lang=es), [5](https://photos.google.com/share/AF1QipPUhHweMG3cpSQ2CRL0scB2KtIKi2D6UFLrLtVuBNpr_z-UWsqce6nzZGmkWi5UWA?pli=1&key=QlZfb2Rtd05vbXRiVklUSldiUGVTSVMxd2hINXJn)
- [Divisibilitat](https://twitter.com/mmart659/status/1460596098226171905?t=rZpJj-JIGugw99cIjEqnpg&s=09), [1](https://twitter.com/lolamenting/status/1460598743951187973?t=ZlG_EY3_dXoGeSCKLBStJQ&s=09), [2](https://drive.google.com/file/d/13_l0QrvfzHkTiZYQLjIuvS_e8nb4Gr7e/view)
- [Irracionales](https://twitter.com/smaccarrone/status/1463569011254472713?t=ple-EDiPbRgQzoznNQBCJw&s=09)
- [Enters](https://neoparaiso.com/imprimir/material-de-Negativos), [1](https://puntmat.blogspot.com/2011/06/conjectura-de-collatz-una-activitat-de.html), [2](http://puntmat.blogspot.com/2012/11/quadrat-magics-i-nombres-enters.html), [3](https://www.maths4everything.com/copia-de-actproc), [4](http://puntmat.blogspot.com/2020/09/practica-productiva-sumes-i-restes-de.html)
- [Algunos juegos de Conway](https://juegosydesafiosmatematicos.com/contenido.aspx?id=33&idSeccion=3)

- [Problemes de mates amb Programació](https://projecteuler.net/archives), [2](https://www.codebymath.com/index.php/welcome)
- [Programem matemátiques amb snap!](https://docs.google.com/presentation/d/e/2PACX-1vQPnQXRGEoAE61PJHT1JpXeUhL6eS_YWOpZ19yyguq-3wsG5jDRi-RRbCU2tx2bVxC9uS6iRvQUN03W/pub?start=false&loop=false&delayms=3000#slide=id.gf37d89184d_0_108)
- [Geometric Puzzles](https://drive.google.com/file/d/1hVP8tLURVDphmHsphz5BQLVzHCeTts29/view), [1](https://twitter.com/brilliantorg/status/1315351925966204930?s=09), [2](https://twitter.com/sonukg4india/status/1499742718267772930?t=WKY6HCb1WskdaFERBdLQdw&s=09)
- [Visió Espacial](https://twitter.com/lolamenting/status/1458361416503672840?t=bdWz4uyb5vBXuvLy_1XETQ&s=09)
- [Rectes](https://teacher.desmos.com/activitybuilder/custom/5f8928490478200c707c7a0b?lang=es)
- [Enters](https://twitter.com/pbeltranp/status/1253953129177059328?s=09)
- [Aritmètica i Algebra](https://sites.google.com/xtec.cat/cesire-matematiques-campanyes/investiguem/dun-problema-de-recreaci%C3%B3-a-explorar-i-investigar)
- [Cryptarithms](https://nrich.maths.org/cryptarithms)

#### Geometria
- [Pitagores](https://twitter.com/pbeltranp/status/1409206394688086019?t=aTS3eo_RvYu0qu-Zk3Wo2w&s=35)
- [Mateix perímetre?](https://twitter.com/giftedHKO/status/1485349911520493569)
- [Area](https://twitter.com/KH2020H0/status/1610679344447328276?t=iisww21Sn2844IZLkvjBjg&s=35)
- [Cercles](https://twitter.com/KH2020H0/status/1606481709062553603?t=9haNmaVuZCxmKXsyCMIBxg)

#### Coses Mates

- [Dimensions i competències](assets/recursos/MatesDimensions_innovamat.jpg)
- [Àmbit Matemàtic](assets/recursos/MatesÀmbit Matemàtic.pdf)
- Laboratori de Mates: [matematiquesmarines](https://matematiquesmarines.blogspot.com/), [Laboratori de Matemàtiques Cesire](https://sites.google.com/xtec.cat/cesire-matematiques-campanyes/laboratori-de-matem%C3%A0tiques), [Memòria Anton Aubanell](http://www.xtec.cat/~aaubanel/Memoria/Memoria.pdf), [Nrich explicat pel cesire](https://sites.google.com/xtec.cat/cesire-matematiques-campanyes/dimensi%C3%B3-web/nrich), [PuntMat explicat pel cesire](https://sites.google.com/xtec.cat/cesire-matematiques-campanyes/dimensi%C3%B3-web/puntmat?authuser=0), [Calaix pel cesire](https://sites.google.com/xtec.cat/cesire-matematiques-campanyes/dimensi%C3%B3-web/calaix-ie), [Y me llevo una](https://capitanswing.com/libros/y-me-llevo-una/), [Jocs d'estar per casa](https://www.vilaweb.cat/etiqueta/jocs-destar-per-casa/), [Square it](https://nrich.maths.org/squareit), [D'un sol tall](https://calaix2.blogspot.com/2015/11/dun-sol-tall-1.html), [D'un sol tall 2](https://calaix2.blogspot.com/2015/11/dun-sol-tall-2.html), [Puzzle Hexagonal](https://www.geogebra.org/m/scuk6ga4), [Rectangles d'igual perímetre, àrees i funcions](https://apliense.xtec.cat/arc/node/29113), [MathWalks](https://sites.google.com/powayusd.com/math-walks/home),
- [Jocs d'estrategia](https://twitter.com/srdelafu/status/1445039608476094470?t=IcyBc3U4LmvzMQAFXFWarw&s=03),[2](/assets/recursos/Jocs d'estrategia/)
- [Primers dies](https://mathequalslove.net/first-week-of-school-activities/#Lets_Make_Squares)
- [Problemes guais](https://twitter.com/pbeltranp/status/1417875549339406339), [combinatoria](https://twitter.com/mmart659/status/1397566020219461633), [numeracio](https://nrich.maths.org/6499), [2](https://www.epriego.net/educa/game-cifras.asp)
- [PlaywithyourMath](https://playwithyourmath.com/)

#### Proporcionalitat

- [Regla de tres](https://twitter.com/pbeltranp/status/1386273501443670016), [Comparación y orden de fracciones](https://twitter.com/pbeltranp/status/1332978320280088576?t=4t0YAK-3R-Nsq2BAm7eEqQ&s=19), [2](https://twitter.com/pbeltranp/status/1446919670209159169)
-[Proporcionalitat](https://teacher.desmos.com/activitybuilder/custom/61710fe92f06505672b93b60?lang=es)
-[Percentatges](https://twitter.com/david_meldor/status/1318498567401689088?s=09)

#### Thinking Classrooms

- [Building Thinking Classrooms](https://www.buildingthinkingclassrooms.com/resources)


#### Bastides per dissenyar projectes

- De'n Jordi Domenech [canvasABP2](assets/recursos/BastidacanvasABP2.odp), [canvasABP2.pdf](assets/recursos/BastidacanvasABP2.pdf), [canvasABPSTEAMCAST](assets/recursos/BastidacanvasABPSTEMCAST.odp), [portafoli](assets/recursos/BastidaportfolioABP2.pdf), [És competencial?](assets/recursos/BastidaFull-indicadors-unitats-competencials.pdf), [Pàgines del Jordi](https://docs.google.com/document/d/e/2PACX-1vS3NNGu3fsUkwvMyWhuzciUU7R9N28GmsMPcERoZCiKCv-v3t1S0pQGPKtILc8R8Id445WR5JOhqp7O/pub)
- Xarxa de Competències Bàsiques [Treball per projectes - Aprenentatge Autèntic](assets/recursos/BastidaTreball per projectes_ aprenentatge autèntic (Juliol 2019).odt)
- [Capacitem l'institut](assets/recursos/BastidaCapacitemlinstitut.pdf)
- [Scrum](https://www.pblworks.org/blog/why-i-scrum-using-project-management-tool-pbl)
- [Les 5 accions de l'aprenentatge](https://twitter.com/RosaSensatAxA/status/1691714270759674125)

#### Objectius d'aprenentatge

- Xarxa de Competències Bàsiques [Compartir Objectius](assets/recursos/ObjectiusCompartir objectius.pdf), [Objectius d'aprenentatge, criteris i indicadors d'avaluació](assets/recursos/Objectius d'aprenentatge, criteris i indicadors d'avaluació.pdf), [Programacions ESO](assets/recursos/Objectius20180302ProgramacionsESO.pdf), [Com definim objectius 'aprenentatge](https://tecnocentres.org/blog/2022/03/31/com-definim-els-objectius-daprenentatge/), [1](https://twitter.com/smaccarrone/status/1171360243055497216)

#### Avaluació

- Xarxa de Competències Bàsiques [Rubrica](assets/recursos/Avaluaciorubrica.pdf), [Carpeta Aprenentatge](assets/recursos/Avaluaciocarpeta-aprenentatge.pdf), [Autoavaluacio i Coavaluacio](assets/recursos/AvaluacioAutoavaluació i coavaluació.pdf), [Les rúbriques per a una avaluació plantejada com a aprenentatge](assets/recursos/AvaluacioLes rúbriques per a una avaluació plantejada com a aprenentatge.pdf), [Decaleg de l'Avaluació Formativa](assets/recursos/AvaluacioDecaleg-AxA.pdf)
- [Avaluació Reguladora](https://twitter.com/crptgn/status/1605481333999820800?t=lvyCbwoZgIIxDhZ8HuUbLw&s=35)


#### Aprenentatge cooperatiu

- Llibres: [9 ideas clave aprendizaje coorperativo](https://twitter.com/MarianaMorale19/status/1275037551359909889), [Entramado](https://twitter.com/MarianaMorale19/status/1276432304156676096)

#### Física i Química

- [Fisica i Quimica](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/), [1](https://didacticafisicaquimica.es/)

#### Informàtica

- [Curriculum Informatica](https://teachcomputing.org/curriculum)

#### Tecnologia

- [Projectes de Pau Folch](https://sites.google.com/view/electronica-programaci-arduino/inici), [1](https://twitter.com/paufolchsax/status/1435266430816169984?s=03)
- [Sonic Pi tutorials](https://www.youtube.com/playlist?list=PLxJoOXhg8m5LbBzczDCeZ4wzky1K578SS), [2](https://www.youtube.com/playlist?list=PLIsdHp2z9wFlu3MRII0eS5NysniOOnXL5), [3](https://dev.to/obitodarky/create-music-with-code-4d8o), [4](https://dev.to/obitodarky/create-music-with-code-2-57je)
- [Pensament computacional i Mates](https://educaixa.org/es/-/hellobebras)

##### Gènere

- [Llista de llibres](https://twitter.com/albablanco29/status/1376079770367242240?s=09)
[Educació Sexual Feminista](https://twitter.com/sidastudi/status/1252628023775498242), [1](http://salutsexual.sidastudi.org/resources/inmagic-img/DD59548.pdf), [2](http://salutsexual.sidastudi.org/resources/inmagic-img/DD59545.pdf)
- [Guia de comunicació inclusiva](https://ajuntament.barcelona.cat/guia-comunicacio-inclusiva/)

#### Feminisme

- [TED](https://www.youtube.com/watch?v=7n9IOH0NvyY)

#### Justicia Social

- [Recursos Canvi Climàtic](https://docs.google.com/document/d/18cfaK5v8rI9xdc4Qj0E_Ik0-c8TYA7R9n9m5Isi7Zf8/edit)
- [Kaidara Recursos](https://www.kaidara.org/)
